﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MAMS
{
    class Sources
    {
        #region 选择列表

        /// <summary>
        /// ListBox数据合集类
        /// </summary>
        public class Accinfo
        {
            public string ID { get; set; }
            public string Name { get; set; }
        }

        /// <summary>
        /// ListBox数据合集
        /// </summary>
        public static ObservableCollection<Accinfo> _AccNameList = new ObservableCollection<Accinfo>();

        /// <summary>
        /// ListBox数据绑定
        /// </summary>
        public static ObservableCollection<Accinfo> AccNameList { 
            get 
            {
                return _AccNameList; 
            }
            set
            {
                _AccNameList = value;
            }
        }

        /// <summary>
        /// 企业信息源数据
        /// </summary>
        public static string[] Acclists { get; set; }

        /// <summary>
        /// 载入企业数据
        /// </summary>
        public static void LoadAccList()
        {
            //清空ListBox资源
            AccNameList.Clear();

            //刷新源数据
            Acclists = SqlLite.GetAccList();

            //判断源数据数量
            if (Acclists.Length > 0)
            {
                //遍历源数据
                foreach (string accinfo in Acclists)
                {
                    //拆分数组
                    string[] accinfos = accinfo.Split(',');

                    //添加到ListBox资源
                    AccNameList.Add(new Accinfo
                    {
                        ID = accinfos[0],
                        Name = accinfos[1]
                    });
                }
            }
        }

        #endregion
    }
}
