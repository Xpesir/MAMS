using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Panuon.UI.Silver;
using static MAMS.AccInfo;

namespace MAMS
{
    /// <summary>
    /// AccEdit_UI.xaml 的交互逻辑
    /// </summary>
    public partial class AccEdit_UI : WindowX
    {
        //声明变量
        public bool EditMode { get; set; }

        public string ID { get; set; }


        public AccEdit_UI()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 保存按钮点击事件
        /// </summary>
        private void SaveAccInfo(object sender, RoutedEventArgs e)
        {
            #region 数据校验
            
            //自定名称
            if (ZdName.Trim().Length < 3)
            {
                MessageBoxX.Show("自定名称不能少于3个字符");
                return;
            }

            //企业名称
            if (Nsrmc.Trim().Length < 3)
            {
                MessageBoxX.Show("企业名称不能少于3个字符");
                return;
            }

            //企业税号
            if (!VNsrsbh(Nsrsbh.Trim()))
            {
                MessageBoxX.Show("纳税人识别号错误！");
                return;
            }

            //账号
            if (User.Trim().Length < 2)
            {
                MessageBoxX.Show("登录账号不能少于2个字符");
                return;
            }

            #endregion

            if (EditMode == true)
            {
                //密码
                if (Password == null || Password.Length <= 0)
                {
                    SqlLite.UpAccInfo(ID, ZdName.Trim(), Nsrmc.Trim(), Nsrsbh.Trim(), User.Trim());
                    
                }
                else
                {
                    SqlLite.UpAccInfo(ID, ZdName.Trim(), Nsrmc.Trim(), Nsrsbh.Trim(), User.Trim(), Password);
                }
            }
            else
            {
                //密码
                if (Password.Length < 6)
                {
                    MessageBoxX.Show("登录密码不能少于6个字符");
                    return;
                }
                SqlLite.AddAcc(ZdName.Trim(), Nsrmc.Trim(), Nsrsbh.Trim(), User.Trim(), Password);
            }

            Close();
        }

        /// <summary>
        /// 税号合法判定
        /// </summary>
        /// <param name="nsrsbh">税号</param>
        /// <returns>合法：true，非法：false</returns>
        public static bool VNsrsbh(string nsrsbh)
        {
            //[0-9A-HJ-NPQRTUWXY]{2}\d{6}[0-9A-HJ-NPQRTUWXY]{10}
            Regex regex = new Regex(@"[0-9A-HJ-NPQRTUWXY]{2}\d{6}[0-9A-HJ-NPQRTUWXY]{10}");
            if (regex.IsMatch(nsrsbh))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 窗体初始化完成执行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowX_ContentRendered(object sender, EventArgs e)
        {
            if (EditMode)
            {
                Title = "编辑企业账号信息";
                string[] infos = SqlLite.GetAccInfo(ID);
                if (infos.Length <= 0)
                {
                    MessageBoxX.Show("没有查询到账号信息");
                    Close();
                }

                ZdName = infos[0];
                Nsrmc = infos[1];
                Nsrsbh = infos[2];
                User = infos[3];
                //Password = infos[4];
            }
            else
            {
                ZdName = Nsrmc = Nsrsbh = User = Password = null;
            }
        }
    }

    /// <summary>
    /// 账号信息类
    /// </summary>
    public class AccInfo
    {
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        public static string zdname;
        public static string nsrmc;
        public static string nsrsbh;
        public static string user;
        public static string password;

        
        public static string ZdName
        {
            get { return zdname; }
            set
            {
                zdname = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(ZdName)));
            }
        }

        public static string Nsrmc
        {
            get { return nsrmc; }
            set
            {
                nsrmc = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(Nsrmc)));
            }
        }

        public static string Nsrsbh
        {
            get { return nsrsbh; }
            set
            {
                nsrsbh = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(Nsrsbh)));
            }
        }

        public static string User
        {
            get { return user; }
            set
            {
                user = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(User)));
            }
        }

        public static string Password
        {
            get { return password; }
            set
            {
                password = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(Password)));
            }
        }
    }
}
