﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAMS
{
    class SqlLite
    {
        private static string DBPath = AppDomain.CurrentDomain.BaseDirectory + "\\NAMS.NAMS";
        private static readonly SQLiteConnection DB = new SQLiteConnection("data source=" + DBPath);
        private static Encrypt encrypt = new Encrypt();

        /// <summary>
        /// 判断数据库是否存在
        /// </summary>
        /// <returns>存在：true，不存在：false</returns>
        public static bool IsDB()
        {
            if (File.Exists(DBPath))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 创建数据库
        /// </summary>
        public static void CreateDB()
        {
            if (!IsDB())
            {
                SQLiteConnection cn = new SQLiteConnection("data source=" + DBPath);
                cn.Open();
                SQLiteCommand cmd = new SQLiteCommand
                {
                    Connection = cn,
                };
                //创建表
                cmd.CommandText = @"CREATE TABLE IF NOT EXISTS AccList(ID INTEGER PRIMARY KEY AUTOINCREMENT ,
                                                                        Name varchar(64) NOT NULL ,
                                                                        Nsrmc varchar(64) NOT NULL ,
                                                                        Nsrsbh varchar(64) NOT NULL ,
                                                                        User varchar(64) NOT NULL ,
                                                                        Password varchar(64) NOT NULL);";

                cmd.ExecuteNonQuery();
                cn.Close();
            }
            else
            {
                throw new Exception("创建数据库异常：数据库已存在！");
            }
        }

        /// <summary>
        /// 执行Sql语句
        /// </summary>
        /// <param name="Sql">Sql语句</param>
        private static void RunSql(string Sql)
        {
            try
            {
                DB.Open();
                SQLiteCommand cmd = new SQLiteCommand
                {
                    Connection = DB,
                    CommandText = Sql,
                };
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("执行失败：" + ex.Message);
            }
            finally
            {
                DB.Close();
            }
        }

        /// <summary>
        /// 添加账号信息
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="User"></param>
        /// <param name="Password"></param>
        public static void AddAcc(string Name, string Nsrsmc, string Nsrsbh,string User,string Password)
        {
            string sql = string.Format(@"INSERT INTO AccList (Name,Nsrmc,Nsrsbh,User,Password)
                                                     VALUES('{0}','{1}','{2}','{3}','{4}');"
                                        , Name,Nsrsmc,Nsrsbh,User, encrypt.EncryptString(Password));
            RunSql(sql);
        }

        /// <summary>
        /// 删除账号
        /// </summary>
        /// <param name="ID"></param>
        public static void DelAcc(string ID)
        {
            RunSql("DELETE FROM AccList WHERE ID = '" + ID + "'");
        }

        /// <summary>
        /// 更新账号信息
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Name"></param>
        /// <param name="Nsrsmc"></param>
        /// <param name="Nsrsbh"></param>
        /// <param name="User"></param>
        /// <param name="Password"></param>
        public static void UpAccInfo(string ID,string Name, string Nsrsmc, string Nsrsbh, string User, string Password)
        {
            string sql = string.Format(@"UPDATE AccList SET 
                                                          Name = '{0}',
                                                          Nsrmc = '{1}',
                                                          Nsrsbh = '{2}',
                                                          User = '{3}',
                                                          Password = '{4}' 
                                                        WHERE 
                                                          ID = '{5}';"
                                        , Name, Nsrsmc, Nsrsbh, User, encrypt.EncryptString(Password), ID);
            RunSql(sql);
        }

        /// <summary>
        /// 更新账号信息
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Name"></param>
        /// <param name="Nsrsmc"></param>
        /// <param name="Nsrsbh"></param>
        /// <param name="User"></param>
        public static void UpAccInfo(string ID, string Name, string Nsrsmc, string Nsrsbh, string User)
        {
            string sql = string.Format(@"UPDATE AccList SET 
                                                          Name = '{0}',
                                                          Nsrmc = '{1}',
                                                          Nsrsbh = '{2}',
                                                          User = '{3}'
                                                        WHERE 
                                                          ID = '{4}';"
                                        , Name, Nsrsmc, Nsrsbh, User,ID);
            RunSql(sql);
        }

        /// <summary>
        /// 获取登录信息
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>数组，税号，账号，密码</returns>
        public static string[] GetLoginInfo(string ID)
        {
            ArrayList arrayList = new ArrayList();
            SQLiteConnection DB = new SQLiteConnection("data source=" + DBPath);
            string Sql = string.Format("SELECT Nsrsbh,User,Password FROM AccList WHERE ID = '{0}'", ID);
            try
            {
                DB.Open();
                SQLiteCommand cmd = new SQLiteCommand
                {
                    Connection = DB,
                    CommandText = Sql,
                };

                SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        arrayList.Add(reader["Nsrsbh"].ToString());
                        arrayList.Add(reader["User"].ToString());
                        arrayList.Add(encrypt.DecryptString(reader["Password"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("获取登录信息失败：" + ex.Message);
            }
            finally
            {
                DB.Close();
            }
            return (string[])arrayList.ToArray(typeof(string));
        }

        /// <summary>
        /// 获取账号信息
        /// </summary>
        public static string[] GetAccInfo(string ID)
        {
            ArrayList arrayList = new ArrayList();
            SQLiteConnection DB = new SQLiteConnection("data source=" + DBPath);
            string Sql = string.Format("SELECT * FROM AccList WHERE ID = '{0}'", ID);
            try
            {
                DB.Open();
                SQLiteCommand cmd = new SQLiteCommand
                {
                    Connection = DB,
                    CommandText = Sql,
                };

                SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        arrayList.Add(reader["Name"].ToString());
                        arrayList.Add(reader["Nsrmc"].ToString());
                        arrayList.Add(reader["Nsrsbh"].ToString());
                        arrayList.Add(reader["User"].ToString());
                        arrayList.Add(encrypt.DecryptString(reader["Password"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("获取账号信息失败：" + ex.Message);
            }
            finally
            {
                DB.Close();
            }
            return (string[])arrayList.ToArray(typeof(string));
        }

        /// <summary>
        /// 获取登录列表
        /// </summary>
        /// <returns></returns>
        public static string[] GetAccList()
        {
            ArrayList arrayList = new ArrayList();
            SQLiteConnection DB = new SQLiteConnection("data source=" + DBPath);
            string Sql = "SELECT ID,Name FROM AccList";
            try
            {
                DB.Open();
                SQLiteCommand cmd = new SQLiteCommand
                {
                    Connection = DB,
                    CommandText = Sql,
                };

                SQLiteDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        arrayList.Add(string.Format("{0},{1}",reader["ID"].ToString(), reader["Name"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("获取账号列表失败：" + ex.Message);
            }
            finally
            {
                DB.Close();
            }
            return (string[])arrayList.ToArray(typeof(string));
        }
    }
}
