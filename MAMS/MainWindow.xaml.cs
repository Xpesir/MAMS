﻿using CefSharp;
using CefSharp.Wpf;
using Panuon.UI.Silver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MAMS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : WindowX
    {
        /// <summary>
        /// 页面是否载入完成，true：载入完成
        /// </summary>
        bool FrameLoadEnd = false;

        public MainWindow()
        {
            InitializeComponent();

            //初始化浏览器
            BrowserLoaded();
        }

        #region 浏览器

        /// <summary>
        /// 浏览器初始化
        /// </summary>
        private void BrowserLoaded()
        {
            Browser.MenuHandler = new MenuHandler();
        }

        /// <summary>
        /// cef菜单事件
        /// </summary>
        public class MenuHandler : IContextMenuHandler
        {
            void IContextMenuHandler.OnBeforeContextMenu(CefSharp.IWebBrowser browserControl, CefSharp.IBrowser browser, CefSharp.IFrame frame, CefSharp.IContextMenuParams parameters, CefSharp.IMenuModel model)
            {
                model.Clear();
            }

            bool CefSharp.IContextMenuHandler.OnContextMenuCommand(CefSharp.IWebBrowser browserControl, CefSharp.IBrowser browser, CefSharp.IFrame frame, CefSharp.IContextMenuParams parameters, CefSharp.CefMenuCommand commandId, CefSharp.CefEventFlags eventFlags)
            {
                //throw new NotImplementedException();
                return false;
            }

            void CefSharp.IContextMenuHandler.OnContextMenuDismissed(CefSharp.IWebBrowser browserControl, CefSharp.IBrowser browser, CefSharp.IFrame frame)
            {
                //throw new NotImplementedException();
            }

            bool CefSharp.IContextMenuHandler.RunContextMenu(CefSharp.IWebBrowser browserControl, CefSharp.IBrowser browser, CefSharp.IFrame frame, CefSharp.IContextMenuParams parameters, CefSharp.IMenuModel model, CefSharp.IRunContextMenuCallback callback)
            {
                return false;
            }
        }

        /// <summary>
        /// 页面载入完成
        /// </summary>
        private void Browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            /*
                                                                    document.getElementById('nc_1_n1z').setAttribute('class','nc_iconfont btn_ok');
                                                                    document.getElementById('nc_1__scale_text').setAttribute('class','scale_text slidetounlock nc-align-center scale_text2');
                                                                    document.getElementsByClassName('nc-lang-cnt')[1].setAttribute('data-nc-lang','_yesTEXT');
                                                                    document.getElementById('nc_1__scale_text').innerHTML = '<span class=""nc-lang-cnt"" data-nc-lang=""_yesTEXT""><b>验证通过</b></span>';
                                                                    document.getElementById('nc_1__imgCaptcha_img').children[0].click();
             */

            //关闭提示以及切换到密码登陆
            Browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync(@"
                                                                    document.getElementsByClassName('layui-layer-btn1')[0].click();
                                                                    $('#smdl').hide();
                                                                    $('#smdl_QieHuan').show();
                                                                    $('#mmdl').show();
                                                                    $('#mmdl_QieHuan').hide();
                                                                    $('#loginQieHuanImg_gddzswjmmdl').hide();
                                                                    $('#loginQieHuanImg_gddzswjsmdl').show();
                                                                    $('#tabtit').show();
                                                                    $('#upLoginForm').show();
                                                                    $('#ewmLoginForm').hide();
                                                                    clearInterval(timer);
                                                                    loginTypeTab('qyLogin');
                                                                    isGo = true;
                                                                    document.getElementById('nc_1__bg').style.cssText='width: 316px;';
                                                                    document.getElementById('nc_1_n1z').style.cssText='left: 316px;';
                                                                    ");

            //载入完成状态设置为true
            FrameLoadEnd = true;

        }
        #endregion

        #region 标题栏
        private void Help(object sender, RoutedEventArgs e)
        {
            Process.Start("https://gitee.com/Xpesir/MAMS/wikis");
        }


        private void PageReLoad(object sender, RoutedEventArgs e)
        {
            Browser.Reload();
        }

        private void HomeLoad(object sender, RoutedEventArgs e)
        {
            //载入完成状态设置为false
            FrameLoadEnd = false;

            //重新载入网址
            Browser.Load("https://etax.guangdong.chinatax.gov.cn/sso/login?service=https://etax.guangdong.chinatax.gov.cn/xxmh/html/index_login.html?bszmFrom=1");
        }

        private void PageBack(object sender, RoutedEventArgs e)
        {
            Browser.Back();
        }

        private void PageForward(object sender, RoutedEventArgs e)
        {
            Browser.Forward();
        }

        private void AddQQ(object sender, RoutedEventArgs e)
        {
            Process.Start("https://jq.qq.com/?_wv=1027&k=y1RcVgdw");
        }
        #endregion

        #region 企业列表

        /// <summary>
        /// 刷新企业信息
        /// </summary>
        private void LoadAccList()
        {
            ListS.Text = null;
            Sources.LoadAccList();
        }

        #region 操作边栏

        /// <summary>
        /// 重载企业列表
        /// </summary>
        private void ReloadAccList(object sender, RoutedEventArgs e)
        {
            LoadAccList();
        }

        /// <summary>
        /// 添加企业
        /// </summary>
        private void AddAcc(object sender, RoutedEventArgs e)
        {
            AccEdit_UI accEdit_UI =  new AccEdit_UI();
            accEdit_UI.ShowActivated = true;
            accEdit_UI.ShowDialog();

            //重载数据
            LoadAccList();
        }

        /// <summary>
        /// 删除企业
        /// </summary>
        private void DelAcc(object sender, RoutedEventArgs e)
        {
            if(AccList.SelectedItem != null)
            {
                string ID = ((Sources.Accinfo)AccList.SelectedItem).ID;
                string Name = ((Sources.Accinfo)AccList.SelectedItem).Name;

               if(MessageBoxX.Show("确认删除【"+Name+"】？","",MessageBoxButton.YesNo,MessageBoxIcon.Warning) == MessageBoxResult.Yes)
                {
                    SqlLite.DelAcc(ID);
                    //重载数据
                    LoadAccList();
                }
            }
        }

        /// <summary>
        /// 编辑企业
        /// </summary>
        private void EditAcc(object sender, RoutedEventArgs e)
        {
            if (AccList.SelectedItem != null)
            {
                string ID = ((Sources.Accinfo)AccList.SelectedItem).ID;
                AccEdit_UI accEdit_UI = new AccEdit_UI
                {
                    EditMode = true,
                    ID = ID
                };
                accEdit_UI.ShowActivated = true;
                accEdit_UI.ShowDialog();

                //重载数据
                LoadAccList();
            }
        }

        #endregion

        #region 企业选取

        /// <summary>
        /// 判断是否网页是否加载完成
        /// </summary>
        public async Task IsLoading()
        {
            while (!FrameLoadEnd)
            {
                await Task.Delay(500);
            }
        }

        /// <summary>
        /// 企业列表双击选择
        /// </summary>
        private async void AccList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (AccList.SelectedIndex != -1)
            {
                //控件不为空 && 鼠标在控件内 && 左键按下
                if (AccList.ItemContainerGenerator.ContainerFromIndex(AccList.SelectedIndex) is ListBoxItem item && item.IsMouseOver && e.ChangedButton == MouseButton.Left)
                {
                    //关闭账户信息抽屉
                    Acc.IsOpen = false;

                    //载入完成状态设置为false
                    FrameLoadEnd = false;

                    //获取账户信息
                    string ID = ((Sources.Accinfo)AccList.SelectedItem).ID;
                    string[] info = SqlLite.GetLoginInfo(ID);

                    //拼装填充JS代码
                    string js = string.Format(@"$('#shxydmOrsbh').val('{0}');$('#userNameOrSjhm').val('{1}');$('#passWord').val('{2}');", info[0], info[1], info[2]);

                    //重新载入网址
                    Browser.Load("https://etax.guangdong.chinatax.gov.cn/sso/login?service=https://etax.guangdong.chinatax.gov.cn/xxmh/html/index_login.html?bszmFrom=1");

                    //等待加载完成
                    await IsLoading();

                    //执行填充JS代码
                    Browser.ExecuteScriptAsync(js);
                }
            }
        }

        /// <summary>
        /// 企业列表检索
        /// </summary>
        private void ListS_TextChanged(object sender, TextChangedEventArgs e)
        {
            //清空ListBox资源
            Sources.AccNameList.Clear();

            //判断源数据不为空
            if (Sources.Acclists.Length > 0)
            {
                //遍历源数据
                foreach (string accinfo in Sources.Acclists)
                {
                    //拆分数组
                    string[] accinfos = accinfo.Split(',');

                    //过滤内容
                    if (accinfos[1].Contains(ListS.Text))
                    {
                        //添加到ListBox资源
                        Sources.AccNameList.Add(new Sources.Accinfo
                        {
                            ID = accinfos[0],
                            Name = accinfos[1]
                        });
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}