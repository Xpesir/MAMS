﻿using System.Windows;
using System.IO;
using Panuon.UI.Silver;

namespace MAMS
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected async override void OnStartup(StartupEventArgs e)
        {
            //设置工作目录
            string Current = System.AppDomain.CurrentDomain.BaseDirectory + "\\Current";
            if (!Directory.Exists(Current))
            {
                Directory.CreateDirectory(Current);
            }
            Directory.SetCurrentDirectory(Current);

            //载入等待窗
            var handler = PendingBoxX.Show("检查更新 ...", "", false);

            //检查更新
            await Updata.Update();

            handler.UpdateMessage("初始化 ...");

            //判断数据库是否存在，不存在则创建
            if (!SqlLite.IsDB())
            {
                SqlLite.CreateDB();
            }

            string filename = MachineCode.GetCpuInfo();
            if (!File.Exists(filename))
            {
                File.Create(filename);
            }

            //载入数据
            Sources.LoadAccList();

            //关闭等待窗
            handler.Close();

            base.OnStartup(e);
        }
    }
}
