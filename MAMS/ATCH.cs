﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MAMS
{
    class ATCH
    {
        public static readonly DependencyProperty PanelProperty = DependencyProperty.RegisterAttached("Panel", typeof(Panel), typeof(ATCH), new PropertyMetadata(null));

        public static void SetPanel(DependencyObject d, Panel value) => d.SetValue(PanelProperty, value);

        public static Panel GetPanel(DependencyObject d) => (Panel)d.GetValue(PanelProperty);


        public static readonly DependencyProperty NameProperty = DependencyProperty.RegisterAttached("Name", typeof(FrameworkElement), typeof(ATCH), new PropertyMetadata(null, new PropertyChangedCallback(OnNamePropertyChanged)));

        public static void SetName(DependencyObject d, FrameworkElement value) => d.SetValue(NameProperty, value);

        public static FrameworkElement GetName(DependencyObject d) => (FrameworkElement)d.GetValue(NameProperty);


        private static void OnNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var b = d.GetValue(PanelProperty);
            if (b is null || !(b is Panel) || e.NewValue is null)
                return;
            var panel = b as Panel;
            var web = d as WebBrowser;
            var ui = e.NewValue as FrameworkElement;
            SetRect(panel, web, ui);
            panel.SizeChanged += (sender, args) =>
            {
                SetRect(panel, web, ui);
            };

        }
        private static IntPtr C1;
        private static void SetRect(Panel panel, WebBrowser web, FrameworkElement ui)
        {
            IntPtr handle = web.Handle;
            Api.Win32.DeleteObject(C1);
            Api.Win32.SetWindowRgn(handle, IntPtr.Zero, true);

            Rect PanelRect = new Rect(new Size(panel.ActualWidth, panel.ActualHeight));

            C1 = Api.Win32.CreateRectRgn((int)0, (int)0, (int)PanelRect.BottomRight.X, (int)PanelRect.BottomRight.Y);

            Rect UIRect = new Rect(new Size(ui.ActualWidth, ui.ActualHeight));

            var D1 = (int)ui.TransformToAncestor(panel).Transform(new Point(0, 0)).X;

            var D2 = (int)ui.TransformToAncestor(panel).Transform(new Point(0, 0)).Y;

            var D3 = (int)(D1 + UIRect.Width);

            var D4 = (int)(D2 + UIRect.Height);

            var C2 = Api.Win32.CreateRectRgn(D1, D2, D3, D4);

            Api.Win32.CombineRgn(C1, C1, C2, 4);

            Api.Win32.SetWindowRgn(handle, C1, true);
        }
    }
}
