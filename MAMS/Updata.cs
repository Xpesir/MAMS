﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json.Linq;
using Panuon.UI.Silver;

namespace MAMS
{
    class Updata
    {
        public static async Task Update()
        {
            await Task.Run(() =>
            {
                try
                {
                    Uri uri = new Uri("https://xpesir.gitee.io/mams/updata.json");
                    WebRequest wr = WebRequest.Create(uri);
                    wr.Timeout = 5000;
                    Stream s = wr.GetResponse().GetResponseStream();
                    StreamReader sr = new StreamReader(s, Encoding.UTF8);
                    string json = sr.ReadToEnd();

                    JObject obj = JObject.Parse(json);
                    Version OldVer = new Version(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()),
                            NewVer = new Version(obj["Ver"].ToString());
                    string Log = obj["Log"].ToString();

                    if (NewVer > OldVer)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (MessageBoxX.Show(Log, "发现新版本", MessageBoxButton.YesNo, MessageBoxIcon.Info) == MessageBoxResult.Yes)
                            {
                                Process.Start("https://gitee.com/Xpesir/MAMS/releases/");
                            }

                        });
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            });
        }
    }
}
