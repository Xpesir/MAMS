﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MAMS.Api
{
    class Win32
    {
        [DllImport("user32.dll")]
        public static extern bool SetWindowRgn(IntPtr hWnd, IntPtr hRgn, bool redraw);
        /// <summary>
        /// 创建一个矩形，本来四个参数均为x1 y1 x2 y2 意思为左上角X1，Y1坐标，右下角X2,Y2坐标，但是为了方便WPF使用我则是改了
        /// left意味矩形和左边的距离
        /// top意味着矩形和顶边距离
        /// rectrightbottom_x意味着矩形右下角的X坐标
        /// rectrightbottom_y意味着矩形右下角的Y坐标
        /// </summary>
        /// <param name="Left"></param>
        /// <param name="Top"></param>
        /// <param name="RectRightBottom_X"></param>
        /// <param name="RectRightBottom_Y"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateRectRgn(int Left, int Top, int RectRightBottom_X, int RectRightBottom_Y);

        [DllImport("GDI32.dll")]
        public static extern bool DeleteObject(IntPtr objectHandle);

        [DllImport("gdi32.dll")]
        public static extern int CombineRgn(IntPtr hrgnDst, IntPtr hrgnSrc1, IntPtr hrgnSrc2, int iMode);
        //合并选项:
        //RGN_AND  = 1;
        //RGN_OR   = 2;
        //RGN_XOR  = 3;
        //RGN_DIFF = 4;
        //RGN_COPY = 5; {复制第一个区域}
    }
}
